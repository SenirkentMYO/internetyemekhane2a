﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InternetYemekhane2A
{
    public partial class Uyegirisi : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {if (Session["GirenUye"] == null)
            {
                
                Response.Redirect("Default.aspx");
            }
            else
            {
                lblMesaj.Text = "Hoşgeldiniz";
            }
            List<YemekList> yemekliste = new List<YemekList>();
            SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["baglantiCumlesi"].ToString());
            baglanti.Open();

            string sorguCumlem = "Select * from YemekListesi";

            SqlCommand komut = new SqlCommand(sorguCumlem, baglanti);

            SqlDataReader rows = komut.ExecuteReader();

            while (rows.Read())
            {
                YemekList y = new YemekList();

                y.ID = (int)rows["ID"];
                y.YemekAdi = (string)rows["YemekAdi"];
                y.Fiyati = (string)rows["Fiyati"];


                yemekliste.Add(y);
            }

            baglanti.Close();
            baglanti.Dispose();

            DataList1.DataSource = yemekliste;
            DataList1.DataBind();
        }

        protected void DataList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}