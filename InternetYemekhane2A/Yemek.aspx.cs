﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InternetYemekhane2A
{
    public partial class Yemek : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<YemekArama> listkullanici = new List<YemekArama>();
            SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["baglanticumlesi"].ToString());
            baglanti.Open();

            string sorgu = "Select * from YemekArama";
            SqlCommand komut = new SqlCommand(sorgu, baglanti);
            komut.ExecuteNonQuery();
            SqlDataReader ar = komut.ExecuteReader();


            while (ar.Read())
            {
                YemekArama k = new YemekArama();
                k.ID = (int)ar["ID"];
                k.YemekAdi = (string)ar["YemekAdi"];
                




                listkullanici.Add(k);



            }


            baglanti.Close();
            Repeater1.DataSource = listkullanici;
            Repeater1.DataBind();


        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }
    }
}